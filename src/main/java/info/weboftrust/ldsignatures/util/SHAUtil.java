package info.weboftrust.ldsignatures.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHAUtil {

	private SHAUtil() {

	}

	public static byte[] sha256(String string) {

		MessageDigest digest;

		try {

			digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException ex) {

			throw new RuntimeException(ex.getMessage(), ex);
		}

		return digest.digest(string.getBytes(StandardCharsets.UTF_8));
	}


	public static String sha256String(String string) {

		MessageDigest digest;

		try {

			digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException ex) {

			throw new RuntimeException(ex.getMessage(), ex);
		}

		byte[] hash = digest.digest(string.getBytes(StandardCharsets.UTF_8));
        return bytesToHex(hash);
	}



	private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
